interface IModal {
  'title': string,
  'bodyElement': HTMLElement,
  'onClose': (e: MouseEvent) => void
}

export { IModal };
