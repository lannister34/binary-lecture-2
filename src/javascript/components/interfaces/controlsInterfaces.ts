interface ITrigger {
  'action'?: (isBlocked: Boolean) => void,
  'argument'?: string,
  'conditions'?: string[],
  'nonConditions'?: string[]
}

interface IControls {
  'PlayerOneAttack': string,
  'PlayerOneBlock': string,
  'PlayerTwoAttack': string,
  'PlayerTwoBlock': string,
  'PlayerOneCriticalHitCombination': string[],
  'PlayerTwoCriticalHitCombination': string[]
}

export { ITrigger, IControls };
