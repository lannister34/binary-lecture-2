interface IAttributes {
  [index: string]: string;
}

interface IElement {
  'tagName'?: string,
  'className'?: string,
  'attributes'?: IAttributes
}

export { IAttributes, IElement };
