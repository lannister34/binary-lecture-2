enum FighterPosition {
  left,
  right
}

interface IFighterDetails {
  '_id': string,
  'name': string,
  'health': number,
  'attack': number,
  'defense': number,
  'source': string,
  'position'?: FighterPosition
}

export { IFighterDetails, FighterPosition };
