import { createElement } from '../helpers/domHelper';
import { IFighterDetails, FighterPosition } from './interfaces/fighterInterfaces';
import { IAttributes } from './interfaces/domInterfaces';

export function createFighterPreview(fighter: IFighterDetails, position: FighterPosition) {
  const positionClassName: string = position === FighterPosition.right ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement: HTMLElement = createElement({
    'tagName': 'div',
    'className': `fighter-preview___root ${positionClassName}`,
  });

  if (fighter) {
    const fighterImage: HTMLElement = createElement({
      'tagName': 'img',
      'attributes': {
        'src': fighter.source
      }
    });

    const fighterDetails: HTMLElement = createElement({
      'tagName': 'div',
      'className': 'fighter-preview___info'
    });

    const fighterName: HTMLElement = createElement({
      'tagName': 'span',
      'className': 'fighter-preview___name'
    });

    fighterName.innerHTML = fighter.name;

    //attack

    const attackBlock: HTMLElement = createElement({
      'tagName': 'div',
      'className': 'fighter-preview___stat-block'
    });

    const attackBlank: HTMLElement = createElement({
      'tagName': 'div',
      'className': 'fighter-preview___stat fighter-preview___attack-blank'
    });

    const attackValue: HTMLElement = createElement({
      'tagName': 'div',
      'className': 'fighter-preview___stat fighter-preview___stat-value fighter-preview___attack-value',
      'attributes': {
        'style': "width: " + fighter.attack * 197 / 5 + 'px'
      }
    });

    const attackTextValue: HTMLElement = createElement({
      'tagName': 'span',
      'className': 'fighter-preview___text-value'
    });

    attackTextValue.innerHTML = String(fighter.attack);

    //health

    const healthBlock: HTMLElement = createElement({
      'tagName': 'div',
      'className': 'fighter-preview___stat-block'
    });

    const healthBlank: HTMLElement = createElement({
      'tagName': 'div',
      'className': 'fighter-preview___stat fighter-preview___health-blank'
    });

    const healthValue: HTMLElement = createElement({
      'tagName': 'div',
      'className': 'fighter-preview___stat fighter-preview___stat-value fighter-preview___health-value',
      'attributes': {
        'style': "width: " + fighter.health * 197 / 100 + 'px'
      }
    });

    const healthTextValue: HTMLElement = createElement({
      'tagName': 'span',
      'className': 'fighter-preview___text-value'
    });

    healthTextValue.innerHTML = String(fighter.health);

    //defense

    const defenseBlock: HTMLElement = createElement({
      'tagName': 'div',
      'className': 'fighter-preview___stat-block'
    });

    const defenseBlank: HTMLElement = createElement({
      'tagName': 'div',
      'className': 'fighter-preview___stat fighter-preview___defense-blank'
    });

    const defenseValue: HTMLElement = createElement({
      'tagName': 'div',
      'className': 'fighter-preview___stat fighter-preview___stat-value fighter-preview___defense-value',
      'attributes': {
        'style': "width: " + fighter.defense * 197 / 5 + 'px'
      }
    });

    const defenseTextValue: HTMLElement = createElement({
      'tagName': 'span',
      'className': 'fighter-preview___text-value'
    });

    defenseTextValue.innerHTML = String(fighter.defense);

    //build node tree

    attackBlock.appendChild(attackBlank);
    attackBlock.appendChild(attackValue);
    attackBlock.appendChild(attackTextValue);

    healthBlock.appendChild(healthBlank);
    healthBlock.appendChild(healthValue);
    healthBlock.appendChild(healthTextValue);

    defenseBlock.appendChild(defenseBlank);
    defenseBlock.appendChild(defenseValue);
    defenseBlock.appendChild(defenseTextValue);

    fighterDetails.appendChild(fighterName);
    fighterDetails.appendChild(attackBlock);
    fighterDetails.appendChild(healthBlock);
    fighterDetails.appendChild(defenseBlock);

    fighterElement.appendChild(fighterImage);
    fighterElement.appendChild(fighterDetails);
  }

  return fighterElement;
}

export function createFighterImage(fighter: IFighterDetails) {
  const { source, name }: IFighterDetails = fighter;
  const attributes: IAttributes = {
    'src': source,
    'title': name,
    'alt': name
  };
  const imgElement: HTMLElement = createElement({
    'tagName': 'img',
    'className': 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
