import { controls } from '../../constants/controls';
import { KeyHandler } from '../helpers/keyHandler';
import { createAudioElement } from '../helpers/domHelper';
import { FighterPosition, IFighterDetails } from './interfaces/fighterInterfaces';
import { ITrigger } from './interfaces/controlsInterfaces';

export async function fight(firstFighter: IFighterDetails, secondFighter: IFighterDetails) {
  const arena: HTMLElement | null = document.querySelector('.arena___root');

  if (!arena) {
    throw Error('There is no arena.');
  }

  const kickSound: HTMLAudioElement = createAudioElement({
    'className': 'audio___kick',
    'attributes': {
      'src': '../../../resources/kick.mp3'
    }
  });

  const blockedKickSound: HTMLAudioElement = createAudioElement({
    'className': 'audio___kick-blocked',
    'attributes': {
      'src': '../../../resources/kick-blocked.mp3'
    }
  });

  const criticalKickSound: HTMLAudioElement = createAudioElement({
    'className': 'audio___kick-critical',
    'attributes': {
      'src': '../../../resources/kick-critical.mp3'
    }
  });

  arena.append(kickSound, blockedKickSound, criticalKickSound);

  firstFighter.position = FighterPosition.left;
  secondFighter.position = FighterPosition.right;

  return new Promise((resolve: (fighter: IFighterDetails) => void) => {
    // resolve the promise with the winner when fight is over

    const firstHealthBar: HTMLElement | null = document.getElementById('0-fighter-indicator');
    const secondHealthBar: HTMLElement | null = document.getElementById('1-fighter-indicator');

    if (!(firstHealthBar && secondHealthBar)) {
      throw Error('There is no health bar.');
    }

    let firstHealthPoints: number = firstFighter.health;
    let secondHealthPoints: number = secondFighter.health;

    let firstCritTimer: number | null, secondCritTimer: number | null;

    const makeDamage: (attacker: IFighterDetails, defender: IFighterDetails, isCritical: Boolean, isBlocked: Boolean) => void =
      function(attacker: IFighterDetails, defender: IFighterDetails, isCritical: Boolean, isBlocked: Boolean) {
        const CRIT_DELAY: number = 10000;
        if (isCritical) {
          if (attacker.position === firstFighter.position) {
            if (firstCritTimer) {
              return;
            } else {
              criticalKickSound.play();
              firstCritTimer = window.setTimeout(() => {
                firstCritTimer = null;
              }, CRIT_DELAY)
            }
          } else {
            if (secondCritTimer) {
              return;
            } else {
              criticalKickSound.play();
              secondCritTimer = window.setTimeout(() => {
                secondCritTimer = null;
              }, CRIT_DELAY)
            }
          }
        }

        isBlocked ?
          blockedKickSound.play() :
          kickSound.play();

        const damage: number = getDamage(attacker, defender, isCritical, isBlocked);
        if (!damage) {
          return;
        } else {
          if (defender.position === firstFighter.position) {
            firstHealthPoints -= damage;
            firstHealthPoints < 0 && (firstHealthPoints = 0);
            firstHealthBar.style.width = ((firstHealthPoints * 100) / firstFighter.health) + '%';
          } else {
            secondHealthPoints -= damage;
            secondHealthPoints < 0 && (secondHealthPoints = 0);
            secondHealthBar.style.width = ((secondHealthPoints * 100) / secondFighter.health) + '%';
          }

          if (firstHealthPoints === 0 || secondHealthPoints === 0) {
            handler.removeListeners();
            resolve(firstHealthPoints <= 0 ? secondFighter : firstFighter);
          }
        }
      }

    const triggers: Map<string, ITrigger> = new Map();

    triggers.set(controls['PlayerOneAttack'], {
      'action': makeDamage.bind(null, firstFighter, secondFighter, false),
      'argument': controls['PlayerTwoBlock'],
      'nonConditions': [controls['PlayerOneBlock']]
    });

    triggers.set(controls['PlayerOneBlock'], {});

    controls['PlayerOneCriticalHitCombination'].forEach((key, index, array) => {
      triggers.set(key, {
        'action': makeDamage.bind(null, firstFighter, secondFighter, true),
        'conditions': array
      });
    });

    triggers.set(controls['PlayerTwoAttack'], {
      'action': makeDamage.bind(null, secondFighter, firstFighter, false),
      'argument': controls['PlayerOneBlock'],
      'nonConditions': [controls['PlayerTwoBlock']]
    });

    triggers.set(controls['PlayerTwoBlock'], {});

    controls['PlayerTwoCriticalHitCombination'].forEach((key, index, array) => {
      triggers.set(key, {
        'action': makeDamage.bind(null, secondFighter, firstFighter, true),
        'conditions': array
      });
    });

    const handler: KeyHandler = new KeyHandler(triggers);
  });
}

export function getDamage(attacker: IFighterDetails, defender: IFighterDetails, isCritical: Boolean, isBlocked: Boolean) {
  if (isBlocked) {
    return 0;
  }

  const damage: number = getHitPower(attacker, isCritical);
  const block: number = !isCritical ? getBlockPower(defender) : 0;

  return damage - block > 0 ? damage - block : 0;
}

export function getHitPower(fighter: IFighterDetails, isCritical: Boolean) {
  // return hit power
  return fighter.attack * (isCritical ? 2 : Math.random() + 1);
}

export function getBlockPower(fighter: IFighterDetails) {
  // return block power
  return fighter.defense * (Math.random() + 1);
}
