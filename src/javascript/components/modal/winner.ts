import { showModal } from './modal';
import { createElement } from '../../helpers/domHelper';
import App from '../../app';
import { IFighterDetails } from "../interfaces/fighterInterfaces";

export function showWinnerModal(fighter: IFighterDetails) {
  const bodyElement: HTMLElement = createElement({
    'tagName': 'div',
    'className': 'modal-body'
  });

  const winnerImage: HTMLElement = createElement({
    'tagName': 'img',
    'className': '',
    'attributes': {
      'src': fighter.source
    }
  });

  bodyElement.appendChild(winnerImage);

  showModal({
    'title': fighter.name.toUpperCase() + ' WON!',
    'bodyElement': bodyElement,
    'onClose': function() {
      document.querySelector('.arena___root') ?.remove();
      new App();
    }
  })
}
