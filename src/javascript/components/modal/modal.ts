import { createElement } from '../../helpers/domHelper';
import { IModal } from '../interfaces/modalInterfaces';

export function showModal({ title, bodyElement, onClose = () => { } }: IModal) {
  const root: HTMLElement | null = getModalContainer();

  if (!root) {
    throw Error('There is no modal.')
  }

  const modal: HTMLElement = createModal({ title, bodyElement, onClose });

  root.append(modal);
}

function getModalContainer() {
  return document.getElementById('root');
}

function createModal({ title, bodyElement, onClose }: IModal) {
  const layer: HTMLElement = createElement({ tagName: 'div', className: 'modal-layer' });
  const modalContainer: HTMLElement = createElement({ tagName: 'div', className: 'modal-root' });
  const header: HTMLElement = createHeader(title, onClose);

  modalContainer.append(header, bodyElement);
  layer.append(modalContainer);

  return layer;
}

function createHeader(title: string, onClose: (e: MouseEvent) => void) {
  const headerElement: HTMLElement = createElement({ tagName: 'div', className: 'modal-header' });
  const titleElement: HTMLElement = createElement({ tagName: 'span' });
  const closeButton: HTMLElement = createElement({ tagName: 'div', className: 'close-btn' });

  titleElement.innerText = title;
  closeButton.innerText = '×';

  const close: (e: MouseEvent) => void = (e: MouseEvent) => {
    hideModal();
    onClose(e);
  }
  closeButton.addEventListener('click', close);
  headerElement.append(titleElement, closeButton);

  return headerElement;
}

function hideModal() {
  const modal: Element | null = document.getElementsByClassName('modal-layer')[0];
  modal ?.remove();
}
