import { createElement } from '../helpers/domHelper';
import { createFighterImage } from './fighterPreview';
import { fight } from './fight';
import { showWinnerModal } from './modal/winner';
import { IFighterDetails, FighterPosition } from './interfaces/fighterInterfaces';

export function renderArena(selectedFighters: IFighterDetails[]) {
  const root: HTMLElement | null = document.getElementById('root');
  const [firstFighter, SecondFighter]: IFighterDetails[] = selectedFighters;
  if (!root) {
    throw Error('There is no root.')
  }

  const arena: HTMLElement = createArena(selectedFighters);

  root.innerHTML = '';
  root.append(arena);

  fight(firstFighter, SecondFighter).then(winner => { showWinnerModal(winner) });

}

function createArena(selectedFighters: IFighterDetails[]) {
  const [firstFighter, SecondFighter]: IFighterDetails[] = selectedFighters;
  const arena: HTMLElement = createElement({ tagName: 'div', className: 'arena___root' });
  const healthIndicators: HTMLElement = createHealthIndicators(firstFighter, SecondFighter);
  const fighters: HTMLElement = createFighters(firstFighter, SecondFighter);

  arena.append(healthIndicators, fighters);
  return arena;
}

function createHealthIndicators(leftFighter: IFighterDetails, rightFighter: IFighterDetails) {
  const healthIndicators: HTMLElement = createElement({ tagName: 'div', className: 'arena___fight-status' });
  const versusSign: HTMLElement = createElement({ tagName: 'div', className: 'arena___versus-sign' });
  const leftFighterIndicator: HTMLElement = createHealthIndicator(leftFighter, FighterPosition.left);
  const rightFighterIndicator: HTMLElement = createHealthIndicator(rightFighter, FighterPosition.right);

  healthIndicators.append(leftFighterIndicator, versusSign, rightFighterIndicator);
  return healthIndicators;
}

function createHealthIndicator(fighter: IFighterDetails, position: FighterPosition) {
  const { name }: IFighterDetails = fighter;
  const container: HTMLElement = createElement({ tagName: 'div', className: 'arena___fighter-indicator' });
  const fighterName: HTMLElement = createElement({ tagName: 'span', className: 'arena___fighter-name' });
  const indicator: HTMLElement = createElement({ tagName: 'div', className: 'arena___health-indicator' });
  const bar: HTMLElement = createElement({ tagName: 'div', className: 'arena___health-bar', attributes: { id: `${position}-fighter-indicator` } });

  fighterName.innerText = name;
  indicator.append(bar);
  container.append(fighterName, indicator);

  return container;
}

function createFighters(firstFighter: IFighterDetails, secondFighter: IFighterDetails) {
  const battleField: HTMLElement = createElement({ tagName: 'div', className: `arena___battlefield` });
  const firstFighterElement: HTMLElement = createFighter(firstFighter, FighterPosition.left);
  const secondFighterElement: HTMLElement = createFighter(secondFighter, FighterPosition.right);

  battleField.append(firstFighterElement, secondFighterElement);
  return battleField;
}

function createFighter(fighter: IFighterDetails, position: FighterPosition) {
  const imgElement: HTMLElement = createFighterImage(fighter);
  const positionClassName: string = position === FighterPosition.right ? 'arena___right-fighter' : 'arena___left-fighter';
  const fighterElement: HTMLElement = createElement({
    tagName: 'div',
    className: `arena___fighter ${positionClassName}`,
  });

  fighterElement.append(imgElement);
  return fighterElement;
}
