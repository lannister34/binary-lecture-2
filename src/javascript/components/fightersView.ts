import { createElement } from '../helpers/domHelper';
import { createFightersSelector } from './fighterSelector';
import { IFighterDetails } from './interfaces/fighterInterfaces';

export function createFighters(fighters: IFighterDetails[]) {
  const selectFighter: (event: MouseEvent, fighterId: string) => Promise<void> = createFightersSelector();
  const container: HTMLElement = createElement({
    tagName: 'div',
    className: 'fighters___root'
  });
  const preview: HTMLElement = createElement({
    tagName: 'div',
    className: 'preview-container___root'
  });
  const fightersList: HTMLElement = createElement({
    tagName: 'div',
    className: 'fighters___list'
  });
  const fighterElements: HTMLElement[] = fighters.map((fighter) => createFighter(fighter, selectFighter));

  fightersList.append(...fighterElements);
  container.append(preview, fightersList);

  return container;
}

function createFighter(fighter: IFighterDetails, selectFighter: (event: MouseEvent, fighterId: string) => Promise<void>) {
  const fighterElement: HTMLElement = createElement({
    'tagName': 'div',
    'className': 'fighters___fighter'
  });
  const imageElement: HTMLElement = createElement({
    'tagName': 'IMG',
    'className': 'fighter___fighter-image',
    'attributes': {
      'src': fighter.source,
      'title': fighter.name,
      'alt': fighter.name
    }
  });
  const onClick: (event: MouseEvent) => Promise<void> = (event: MouseEvent) => selectFighter(event, fighter._id);

  fighterElement.append(imageElement);
  fighterElement.addEventListener('click', onClick, false);

  return fighterElement;
}
