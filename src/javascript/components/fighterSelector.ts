import { createElement } from '../helpers/domHelper';
import { renderArena } from './arena';
import versusImg from '../../../resources/versus.png';
import { createFighterPreview } from './fighterPreview';
import { fighterService } from '../services/fightersService';
import { IFighterDetails } from './interfaces/fighterInterfaces';
import { FighterPosition } from './interfaces/fighterInterfaces';

export function createFightersSelector() {
  let selectedFighters: IFighterDetails[] = [];

  return async (event: MouseEvent, fighterId: string) => {
    const fighter: IFighterDetails = await getFighterInfo(fighterId);
    const [playerOne, playerTwo]: IFighterDetails[] = selectedFighters;
    const firstFighter: IFighterDetails = playerOne ?? fighter;
    const secondFighter: IFighterDetails = Boolean(playerOne) ? playerTwo ?? fighter : playerTwo;
    selectedFighters = [firstFighter, secondFighter];

    renderSelectedFighters(selectedFighters);
  };
}

const fighterDetailsMap: Map<string, IFighterDetails> = new Map();

export async function getFighterInfo(fighterId: string) {
  // get fighter info from fighterDetailsMap or from service and write it to fighterDetailsMap

  const fighterData: IFighterDetails | undefined = fighterDetailsMap.get(fighterId);
  if (!fighterData) {
    const fetchedFighterData: IFighterDetails = await fighterService.getFighterDetails(fighterId);
    fighterDetailsMap.set(fighterId, fetchedFighterData);
    return fetchedFighterData;
  }

  return fighterData;
}

function renderSelectedFighters(selectedFighters: IFighterDetails[]) {
  const fightersPreview: HTMLElement | null = document.querySelector('.preview-container___root');
  const [playerOne, playerTwo]: IFighterDetails[] = selectedFighters;
  const firstPreview: HTMLElement = createFighterPreview(playerOne, FighterPosition.left);
  const secondPreview: HTMLElement = createFighterPreview(playerTwo, FighterPosition.right);
  const versusBlock: HTMLElement = createVersusBlock(selectedFighters);

  if (!fightersPreview) {
    throw Error('No root container.');
  }

  fightersPreview.innerHTML = '';
  fightersPreview.append(firstPreview, versusBlock, secondPreview);
}

function createVersusBlock(selectedFighters: IFighterDetails[]) {
  const canStartFight: Boolean = selectedFighters.filter(Boolean).length === 2;
  const onClick: () => void = () => startFight(selectedFighters);
  const container: HTMLElement = createElement({
    tagName: 'div',
    className: 'preview-container___versus-block'
  });
  const image: HTMLElement = createElement({
    'tagName': 'img',
    'className': 'preview-container___versus-img',
    'attributes': { src: versusImg },
  });
  const disabledBtn: string = canStartFight ? '' : 'disabled';
  const fightBtn: HTMLElement = createElement({
    'tagName': 'button',
    'className': `preview-container___fight-btn ${disabledBtn}`,
  });
  fightBtn.addEventListener('click', onClick, false);
  fightBtn.innerText = 'Fight';
  container.append(image, fightBtn);

  return container;
}

function startFight(selectedFighters: IFighterDetails[]) {
  renderArena(selectedFighters);
}
