import { IElement } from '../components/interfaces/domInterfaces';

export function createElement({ tagName = 'div', className = '', attributes = {}  } : IElement) {
  const element: HTMLElement = document.createElement(tagName);

  const classNames: Array<string> = className.split(' ').filter(Boolean);
  element.classList.add(...classNames);

  Object.keys(attributes).forEach((key) => element.setAttribute(key, attributes[key]) );

  return element;
}

export function createAudioElement({ className = '', attributes = {} } : IElement) {
  const audioElement: HTMLAudioElement = new Audio();

  const classNames: Array<string> = className.split(' ').filter(Boolean);
  audioElement.classList.add(...classNames);

  Object.keys(attributes).forEach((key) => audioElement.setAttribute(key, attributes[key]) );

  return audioElement;
}