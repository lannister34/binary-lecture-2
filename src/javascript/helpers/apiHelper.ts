import { fightersDetails } from './mockData';
import { IFighterDetails } from '../components/interfaces/fighterInterfaces';

const API_URL: string = 'https://api.github.com/repos/binary-studio-academy/stage-2-es6-for-everyone/contents/resources/api/';
const useMockAPI: boolean = true;
const defaultOptions: object = {
  'method': 'get'
};

async function fetchData(endpoint: string, options: object = defaultOptions) {
  const url = API_URL + endpoint;
  return fetch(url, options)
    .then((response) => (response.ok ? response.json() : Promise.reject(Error('Failed to load'))))
    .then((result) => JSON.parse(atob(result.content)))
    .catch((error) => {
      throw error;
    });
}

function fetchFighters() {
  if (useMockAPI) {
    return new Promise((resolve: (fighters: IFighterDetails[]) => void, reject: (err: string) => void) => {
      resolve(fightersDetails)
    });
  } else {
    const response: Promise<IFighterDetails[]> = fetchData('fighters.json');
    return response;
  }
}

function fetchFighterDetails(id: string) {
  if (useMockAPI) {
    return new Promise((resolve: (fighter: IFighterDetails) => void, reject: (err: string) => void) => {
      const fighter: IFighterDetails | undefined = fightersDetails.find((it: IFighterDetails) => it._id === id);
      if (!fighter) {
        reject('There is no fighter with this id.');
        return;
      }

      resolve(fighter);
    })
  } else {
    const response: Promise<IFighterDetails> = fetchData(`details/fighter/${id}.json`);
    return response;
  }
}

export { fetchFighters, fetchFighterDetails };
