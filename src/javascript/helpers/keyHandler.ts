import { ITrigger } from '../components/interfaces/controlsInterfaces';

export class KeyHandler {
  private keys: Map<string, boolean>;
  private triggers: Map<string, ITrigger>;
  public removeListeners: () => void;

  constructor(triggers: Map<string, ITrigger>) {
    this.keys = new Map();

    for (let item of triggers) {
      this.keys.set(item[0], false);
    }

    this.triggers = triggers;

    const handleDown: (e: KeyboardEvent) => void = this.keydownHandler.bind(this);
    const handleUp: (e: KeyboardEvent) => void = this.keyupHandler.bind(this);

    this.removeListeners = function() {
      window.removeEventListener('keydown', handleDown, false);
      window.removeEventListener('keyup', handleUp, false);
    };

    window.addEventListener('keydown', handleDown, false);
    window.addEventListener('keyup', handleUp, false);
  }

  keydownHandler(e: KeyboardEvent) {
    if (this.keys.get(e.code) === false) {
      this.keys.set(e.code, true);

      const trigger: ITrigger | undefined = this.triggers.get(e.code);

      if (!trigger || !trigger.action) {
        return;
      }

      if (trigger.conditions ?.some((key: string) => {
        return !this.keys.get(key)
      })) {
        return;
      }

      if (trigger.nonConditions ?.some((key: string) => {
        return this.keys.get(key)
      })) {
        return;
      }

      let arg: boolean = false;

      if (trigger.argument) {
        arg = Boolean(this.keys.get(trigger.argument));
      }

      trigger.action(arg);
    }
  }

  keyupHandler(e: KeyboardEvent) {
    this.keys.has(e.code) && (this.keys.set(e.code, false));
  }

}
