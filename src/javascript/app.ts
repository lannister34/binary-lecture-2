import { createFighters } from './components/fightersView';
import { fighterService } from './services/fightersService';
import { IFighterDetails } from './components/interfaces/fighterInterfaces';

class App {
  constructor() {
    this.startApp();
  }

  static rootElement: HTMLElement | null = document.getElementById('root');
  static loadingElement: HTMLElement | null = document.getElementById('loading-overlay');

  async startApp() {
    try {

      if (App.loadingElement) {
        App.loadingElement.style.visibility = 'visible';
      }

      const fighters: IFighterDetails[] = await fighterService.getFighters();
      const fightersElement: HTMLElement = createFighters(fighters);

      if (!App.rootElement) {
        throw Error('There is no root.')
      }

      App.rootElement.appendChild(fightersElement);
    } catch (error) {
      console.warn(error);
      if (App.rootElement) {
        App.rootElement.innerText = 'Failed to load data';
      }
    } finally {
      if (App.loadingElement) {
        App.loadingElement.style.visibility = 'hidden';
      }
    }
  }
}

export default App;
