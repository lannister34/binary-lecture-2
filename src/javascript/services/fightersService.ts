import { fetchFighters, fetchFighterDetails } from '../helpers/apiHelper';
import { IFighterDetails } from '../components/interfaces/fighterInterfaces';

class FighterService {
  async getFighters() {
    try {
      const response: IFighterDetails[] = await fetchFighters();
      return response;
    } catch (error) {
      throw error;
    }
  }

  async getFighterDetails(id: string) {
    try {
      const response: IFighterDetails = await fetchFighterDetails(id);
      return response;
    } catch (error) {
      throw error;
    }
  }
}

export const fighterService = new FighterService();
